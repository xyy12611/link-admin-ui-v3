import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  Login = '/rest/auth/login',
  Logout = '/rest/auth/logout',
  GetUserInfo = '/rest/auth/getUserInfo',
  TestRetry = '/testRetry',
  ModifyPwd = '/rest/auth/modifyPwd',
  UpdateBasicInfo = '/rest/auth/updateBasicInfo',
}

/**
 * @description: user login api
 */
export function loginApi(params, mode: ErrorMessageMode = 'modal') {
  return defHttp.post(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */
export function getUserInfo() {
  return defHttp.get({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

export function doLogout() {
  return defHttp.get({ url: Api.Logout });
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}

export const modifyPwd = (params) => defHttp.post({ url: Api.ModifyPwd, params });

export const updateBasicInfo = (params) => defHttp.post({ url: Api.UpdateBasicInfo, params });
