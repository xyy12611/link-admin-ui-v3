import { defHttp } from '/@/utils/http/axios';

enum Api {
  getNoticePage = '/rest/notice/getNoticePage',
  addNotice = '/rest/notice/addNotice',
  updateNotice = '/rest/notice/updateNotice',
  deleteNotice = '/rest/notice/deleteNotice',
  getNoticeInfo = '/rest/notice/getNoticeInfo',
}

export const getNoticePage = (params) => defHttp.get({ url: Api.getNoticePage, params });
export const addNotice = (params) => defHttp.post({ url: Api.addNotice, params });
export const updateNotice = (params) => defHttp.post({ url: Api.updateNotice, params });
export const deleteNotice = (params) => defHttp.get({ url: Api.deleteNotice, params });
export const getNoticeInfo = (params) => defHttp.get({ url: Api.getNoticeInfo, params });
