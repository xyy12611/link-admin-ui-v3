import { withInstall } from '/@/utils';
import countTo from './src/CountTo.vue';
/**
 * 数字动画组件
 */
export const CountTo = withInstall(countTo);
