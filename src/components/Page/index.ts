import { withInstall } from '/@/utils';

import pageFooter from './src/PageFooter.vue';
import pageWrapper from './src/PageWrapper.vue';
import pageSearchTableWrappe from './src/PageSearchTableWrappe.vue';

export const PageWrapper = withInstall(pageWrapper);
export const PageFooter = withInstall(pageFooter);
export const PageSearchTableWrappe = withInstall(pageSearchTableWrappe);

export const PageWrapperFixedHeightKey = 'PageWrapperFixedHeight';
